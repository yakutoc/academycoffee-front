var createCustomWindow = function(){
	var CustomWindow = function(){
		this.layer = null;
		this.marker = null;
		this.position = null;
		this.yOffset = 0;
		this.xOffset = 0;
		this.className = 'mapInfoWindow';
		this.container = $('<div></div>').addClass(this.className).append($('<div></div>').addClass(this.className + '-wrapper'));
	};
	/**
	 * Inherit from OverlayView
	 * @type {google.maps.OverlayView}
	 */
	CustomWindow.prototype = new google.maps.OverlayView();
	/**
	 * Called when this overlay is set to a map via this.setMap. Get the appropriate map pane
	 * to add the window to, append the container, bind to close element.
	 * @see CustomWindow.open
	 */
	CustomWindow.prototype.onAdd = function(){
		this.layer = $(this.getPanes().floatPane);
		this.layer.append(this.container);
		/*this.container.find('.map-info-close').on('click', _.bind(function(){
		 // Close info window on click
		 this.close();
		 }, this));*/
	};
	/**
	 * Called after onAdd, and every time the map is moved, zoomed, or anything else that
	 * would effect positions, to redraw this overlay.
	 */
	CustomWindow.prototype.draw = function(){
		var markerIcon = this.marker.getIcon();
		var cHeight = this.container.outerHeight() + (markerIcon.scaledSize ? markerIcon.scaledSize.height : 0);
		var cWidth = this.container.outerWidth() / 2;
		this.position = this.getProjection().fromLatLngToDivPixel(this.marker.getPosition());
		this.container.css({
			'top': this.position.y /*- cHeight*/ - this.yOffset,
			'left': this.position.x /*- cWidth*/ - this.xOffset,
		});
		//console.log(this.container.outerWidth(), this.container.outerHeight());
	};
	/**
	 * Called when this overlay has its map set to null.
	 * @see CustomWindow.close
	 */
	CustomWindow.prototype.onRemove = function(){
		this.container.remove();
	};
	/**
	 * Sets the contents of this overlay.
	 * @param {string} html
	 */
	CustomWindow.prototype.setContent = function(html){
		this.container.find('.' + this.className + '-wrapper').html(html);
	};
	/**
	 * Sets the map and relevant marker for this overlay.
	 * @param {google.maps.Map} map
	 * @param {google.maps.Marker} marker
	 */
	CustomWindow.prototype.open = function(map, marker){
		this.marker = marker;
		this.setMap(map);
	};
	/**
	 * Close this overlay by setting its map to null.
	 */
	CustomWindow.prototype.close = function(){
		this.setMap(null);
	};

	CustomWindow.prototype.isOpen = function(){
		var map = this.getMap();
		return (map !== null && typeof map !== "undefined");
	};

	CustomWindow.prototype.setYOffset = function(offset){
		this.yOffset = offset;
	};

	CustomWindow.prototype.setXOffset = function(offset){
		this.xOffset = offset;
	};

	CustomWindow.prototype.setClassName = function(name){
		this.className = name;
		this.container.attr('class', this.className)
	};

	return new CustomWindow();
};