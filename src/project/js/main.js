// @include customWindow.js
// @include popup.js

var elem = 1;
var error = 0;

var markers = [];
markers[0] = 
	{
		description: 'Новосибирск, ул.&nbsp;Ленина,&nbsp;д.&nbsp;3',
		position: {lat: 55.029566, lng: 82.917351},
		bubble_src: 'https://www.google.com/maps/embed?pb=!1m0!3m2!1sru!2sru!4v1468578899468!6m8!1m7!1siXxcHTk-PgsAAAQ0DGT-8g!2m2!1d55.02960939174973!2d82.91767727874617!3f256.67040429496666!4f-0.6607851440810748!5f0.7820865974627469',
	};
markers[1] =
	{
		description: 'Новосибирск, ул.&nbsp;Вокзальная&nbsp;Магистраль, д.&nbsp;16',
		position: {lat: 55.031398, lng: 82.914230},
		bubble_src: null,
	};
markers[2] =
	{
		description: 'Новосибирск, пр.&nbsp;Карла&nbsp;Маркса, д.&nbsp;47',
		position: {lat: 54.990533, lng: 82.908125},
		bubble_src: 'https://www.google.com/maps/embed?pb=!1m0!3m2!1sru!2sru!4v1468579080896!6m8!1m7!1s8B513NFioIQAAAQ0DexjbA!2m2!1d54.99049474834679!2d82.90803649012776!3f313.0569281357677!4f-10.420784518958357!5f0.7820865974627469',
	};
markers[3] =
	{
	description: 'Красноярск, пр.&nbsp;Карла&nbsp;Маркса, д.&nbsp;147',
	position: {lat: 56.009400, lng: 92.857015},
	bubble_src: null,
};

// $('#formVacancy .popUp-form-select').on('select', function() {
// 	error = 0;
// 	$('#formVacancy .popUp-form-select').each(function() {
// 		if ($(this).val() == 0) {
// 			error = error + 1;
// 		}
// 	});
// 	console.log('error',error);
// 	if (error == 0) {
// 		$('#formVacancy .btn').removeAttr('disabled');
// 		$('#formVacancy .btn').removeClass('-disabled');
// 	} else {
// 		$('#formVacancy .btn').attr('disabled', 'disabled');
// 		$('#formVacancy .btn').addClass('-disabled');
// 	}
// });

var video = function(el) {
	var self = this;
	self.video = el;
	self.src = el.querySelectorAll('source');
	self.randomNumber = function(){
		return (Math.floor(Math.random() * (40 - 1 + 1)) + 1);
	};
	self.playing = false;
	$(self.video).fadeOut(0);
	self.lazyLoading = function(){
		for (var i = 0; i < self.src.length; i++){
			var source = self.src[i].getAttribute('data-src');
			self.src[i].src = source;
		}
		//self.video.currentTime = self.randomNumber();
		self.video.load();
		self.video.addEventListener('canplay', function() {
			if (!self.playing) {
				self.video.currentTime = self.randomNumber();
				//console.log(self.video.currentTime);
				self.video.play();
				$(self.video).fadeIn(500);
				self.playing = true;
			}
		});
	};
	if (window.innerWidth < 768){
	}
	else {
		self.lazyLoading();
	}
	self.stopVideo = function() {
		self.video.pause();
	};
};

var loadingBlock;

$(document).ready(function() {

	$('#messageForUs .popUp-form-textInput, #messageForUs .popUp-form-textarea').keyup(function() {
		error = 0;
		$('#messageForUs .popUp-form-textInput, #messageForUs .popUp-form-textarea').each(function() {
			if ($(this).val() == '') {
				error = error + 1;
			}
		});
		console.log('error',error);
		if (error == 0) {
			$('#messageForUs .btn').removeAttr('disabled');
			$('#messageForUs .btn').removeClass('-disabled');
		} else {
			$('#messageForUs .btn').attr('disabled', 'disabled');
			$('#messageForUs .btn').addClass('-disabled');
		}
	});

	$('#formVacancy .popUp-form-textInput, #formVacancy .popUp-form-textarea').keyup(function() {
		error = 0;
		$('#formVacancy .popUp-form-textInput, #formVacancy .popUp-form-textarea').each(function() {
			if ($(this).val() == '') {
				error = error + 1;
			}
		});
		console.log('error',error);
		if (error == 0) {
			$('#formVacancy .btn').removeAttr('disabled');
			$('#formVacancy .btn').removeClass('-disabled');
		} else {
			$('#formVacancy .btn').attr('disabled', 'disabled');
			$('#formVacancy .btn').addClass('-disabled');
		}
	});

	new popup($('#formVacancy'),$('[data-popup="formVacancy"]'), $('.popUp-form'));
	new popup($('#messageForUs'),$('[data-popup="messageForUs"]'),$('.popUp-form'));
	new popup($('#popupImage'),$('[data-popup="popupImage"]'), $('.popUp-image'));

	$('[data-popup="popupImage"]').addClass('_hover');

	if (document.querySelector('.video')) {
		new video(document.querySelector('.video'));
	}
	$('#nav-icon').click(function () {
		var body = $('body');
		if (body.hasClass('-menuOpened')) {
			body.removeClass('-menuOpened');
			$(this).removeClass('open');
			$('.mobile_menu').fadeOut(200);
		} else {
			body.addClass('-menuOpened');
			$(this).addClass('open');
			$('.mobile_menu').fadeIn(200);
		}
	});

	new tablet($('.cafe_menu_tablet'));

	$(window).on('resize', function () {
		$('.mobile_menu').hide();
		$('#nav-icon').removeClass('open');
	});

	$('.arrow_left').click(function () {
		$('.item_' + elem).fadeOut(300, function () {
			if (elem == 1) {
				elem = 6;
			} else {
				elem = elem - 1;
			}
			$('.item_' + elem).fadeIn(0);
		});
	});

	$('.arrow_right').click(function () {
		$('.item_' + elem).fadeOut(300, function () {
			if (elem == 6) {
				elem = 1;
			} else {
				elem = elem + 1;
			}
			$('.item_' + elem).fadeIn(0);
		});
	});

	var vid = $(window).height() - $('.header').innerHeight();
	if ($('.video-container').innerHeight() > vid) {
		$('.video-container iframe').height(vid);
	}

	$('.events_list_item').click(function() {
		var link = $(this).attr('rel');
		window.location.replace(link);

		var iwOuter = $('.gm-style-iw');
		iwOuter.closest('div').children(':nth-child(1)').children(':nth-child(2)').css({'background-color':'none', 'box-shadow':'none'});
		console.log(iwOuter.closest('div'));
	});

	loadingBlock = new showLoadingBlock();
});

var map;

function initMap() {
	var styles = [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":60}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"lightness":30}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ef8c25"},{"lightness":40}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#b6c54c"},{"lightness":40},{"saturation":-40}]},{}];
	var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
	var isDraggable = $(document).width() > 480 ? true : false;
	var mapOptions = {
		clickableIcons: false,
		draggable: isDraggable,
		scrollwheel: isDraggable,
		zoom: 4,
		center: new google.maps.LatLng(55.045961, 82.913996),
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		}
	};
	map = new google.maps.Map(document.getElementById('map'), mapOptions);
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');

	for(var i=0; i < markers.length; i++)
	{
		markers[i].marker = new google.maps.Marker({
			position: markers[i].position,
			map: map,
			icon: {
				url: './img/static/marker.png',
				size: new google.maps.Size(135, 225),
				scaledSize: new google.maps.Size(135/3, 225/3),
				anchor: new google.maps.Point(135/3/2, (225-60)/3),
			}
		});

		// markers[i].infowindow = new google.maps.InfoWindow({
		//     content: markers[i].description,
		//     maxWidth: 300,
		//     maxHeight: 400,
		// });

		markers[i].infowindow = createCustomWindow();
		markers[i].infowindow.setContent(markers[i].description);
		markers[i].infowindow.setYOffset(70);

		google.maps.event.addListener(markers[i].infowindow, 'domready', function() {
			var iwOuter = $('.gm-style-iw');
			iwOuter.parent().children(':nth-child(1)').children(':nth-child(2)').css({'background-color':'initial', 'box-shadow':'none'});
			iwOuter.parent().children(':nth-child(1)').children(':nth-child(3)').children(':nth-child(1)').children(':nth-child(1)').css({'box-shadow':'none'});
			iwOuter.parent().children(':nth-child(1)').children(':nth-child(3)').children(':nth-child(2)').children(':nth-child(1)').css({'box-shadow':'none'});
		});

		(function(index){
			markers[index].marker.addListener('click', function() {
				for(var i=0; i < markers.length; i++) {
					markers[i].infowindow.close();
				}
				markers[index].infowindow.open(map, markers[index].marker);
				var mapContainer = $('.map_absolute');
				var center = map.getCenter();
				if (markers[index].bubble_src) {
					mapContainer.removeClass('-onlyMap');
					mapContainer.find('.visual iframe').attr('src', markers[index].bubble_src);
				} else {
					mapContainer.addClass('-onlyMap');
				}
				google.maps.event.trigger(map, "resize");
				map.setCenter(center);
			});
		})(i);

		/*map.addListener('zoom_changed', function() {
			for(var i=0; i < markers.length; i++) {
				markers[i].infowindow.close();
			}
		});*/

	}

	markers[0].infowindow.open(map, markers[0].marker);

	google.maps.event.addListener(map, 'click', function() {
        for(var i=0; i < markers.length; i++)
			markers[i].infowindow.close();
    });
}

function myClick(num) {
	for(var i=0; i < markers.length; i++) {
		markers[i].infowindow.close();
	}
	$(window).scrollTop(0);
	var mapContainer = $('.map_absolute');
	var center = markers[num].position;
	if (markers[num].bubble_src) {
		mapContainer.removeClass('-onlyMap');
		mapContainer.find('.visual iframe').attr('src', markers[num].bubble_src);
	} else {
		mapContainer.addClass('-onlyMap');
	}
	google.maps.event.trigger(map, "resize");
	map.setCenter(center);
	map.setZoom(14);
	markers[num].infowindow.open(map, markers[num].marker);
}

var tablet = function(el){
	var self = this;
	self.el = $(el);
	self.tablets = self.el.find('.cafe_menu_tablet_text');
	self.next = self.el.find('.next_menu');
	self.prev = self.el.find('.prev_menu');
	self.showNextTablet = function(){
		var currentTablet = $(this).closest('.cafe_menu_tablet_text');
		var currentTabletIndex = self.tablets.index(currentTablet);
		var index;
		if (currentTabletIndex + 1 > self.tablets.length - 1) {
			index = 0;
			self.tablets.eq(currentTabletIndex).hide();
			self.tablets.eq(index).fadeIn(300);
		} else {
			index = currentTabletIndex + 1;
			self.tablets.eq(currentTabletIndex).hide();
			self.tablets.eq(index).fadeIn(300);
		}
	};
	self.showPrevTablet = function(){
		var currentTablet = $(this).closest('.cafe_menu_tablet_text');
		var currentTabletIndex = self.tablets.index(currentTablet);
		var index;
		if (currentTabletIndex - 1 < 0) {
			index = self.tablets.length - 1;
			self.tablets.eq(currentTabletIndex).hide();
			self.tablets.eq(index).fadeIn(300);
		} else {
			index = currentTabletIndex - 1;
			self.tablets.eq(currentTabletIndex).hide();
			self.tablets.eq(index).fadeIn(300);
		}
	};
	self.prev.on('click touch', self.showPrevTablet);
	self.next.on('click touch', self.showNextTablet);
};

var showSuccessMessage = function(form){
	form.fadeOut(300);
	var popup = form.parent();
	var message = $('<div class="popUp-success"></div>');
	message.html('Спасибо, Ваше сообщение отправлено.');
	$(message).appendTo(popup);
};

var showLoadingBlock = function(){
	var self = this;
	self.content = $('.lesson_absolute_content');
	self.block = $('<div class="loadingBlock"><img src="./img/static/loading.gif" alt=""></div>');
	self.init = function(){
		self.block.appendTo(self.content);
	};
	self.destroy = function(){
		self.block.remove();
	};
};
