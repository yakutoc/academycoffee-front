var popup = function(el, opener, currentPopup){
	var self = $(this);
	self.opener = opener;
	self.close = el.find('.popUp-exit');
	self.currentPopup = el.find(currentPopup);
	self.popupForm = el.find('.popUp-form');
	self.image = el.find('.popUp-image-img');
	self.show = function(){
		$('body').addClass('-popupOpened');
		var src = $(this).attr('data-src');
		self.image.attr('src', src);
		$(el).fadeIn(300);
	};
	self.hide = function () {
		$('body').removeClass('-popupOpened');
		$(el).fadeOut(300);
	};
	self.opener.on('click touch', self.show);
	self.close.on('click touch', self.hide);
	el.on('click touch', function (e) {
		var content = $(e.target).closest(self.currentPopup);
		if (!content.length) {
			self.hide()
		}
	});
};