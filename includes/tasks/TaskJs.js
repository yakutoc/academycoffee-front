import gulp from 'gulp';
import plumber from 'gulp-plumber';
import preprocess from 'gulp-preprocess';
//import babel from 'gulp-babel';

export default class TaskJs
{
	constructor(config = {})
	{
		gulp.task(config.name, () => {
			gulp.src(config.src)
				.pipe(plumber())
				.pipe(preprocess())
				/*.pipe(babel({
					presets: ['es2015']
				}))*/
				.pipe(gulp.dest(config.dest));
		});
	}
}